#!/bin/bash
# parametrage 
rep_conf_firefox="conf_firefox_public"
firefoxesrlocalsettingsjs="$rep_conf_firefox/local-settings.js"
firefoxesrlocalconf="$rep_conf_firefox/mozilla.cfg"
if [ -f $firefoxesrlocalsettingsjs ];then
   if [ -f $firefoxesrlocalconf ];then
      cp -f $firefoxesrlocalsettingsjs /usr/lib/firefox-esr/defaults/pref/
      cp -f $firefoxesrlocalconf /usr/lib/firefox-esr/
   fi
fi

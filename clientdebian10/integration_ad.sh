#!/bin/bash

apt-get -y autoremove --purge ctparental privoxy dnsmasq e2guardian clamav*
apt-get -y install realmd sssd sssd-tools adcli  krb5-user packagekit samba samba-common samba-common-bin samba-libs cifs-utils keyutils libpam-script rsync sudo ntp whiptail expect

#export LANG="fr_FR.UTF-8"  # dé-commenter si besoins
# cp ad_passwordnew_fr_FR.mo /usr/share/locale/fr/LC_MESSAGES/ad_passwordnew.mo
DIRLOCALE=${DIRLOCALE:="/usr/share/locale"}
#chargement des locales. pour la transduction des messages vi gettext
set -a
source /usr/bin/gettext.sh
set +a
export TEXTDOMAINDIR="$DIRLOCALE"
export TEXTDOMAIN="ad_passwordnew"


form_title="Integration au domain"
form_domain="domain :"
form_domain_admin="compte d'integration au domaine :"
form_pwd_domain_admin="Mots de passe :"

export netlogonsmbrep=${netlogonsmbrep:="/media/netlogon/"}
export netlogonlocalrep=${netlogonlocalrep:="/usr/local/etc/netlogon/"}
mkdir -p $netlogonsmbrep
mkdir -p $netlogonlocalrep

kinit_exit_code=1
# obtention du ticket Kerkeros de l'administrateur du domain
while [ "$kinit_exit_code" != "0" ]
do
domain=$(whiptail --title "$form_title"  --inputbox "$(gettext "$form_domain")" 10 60 3>&1 1>&2 2>&3)
if [ "$?" != "0" ] ; then
		exit 1
fi
admindudom=$(whiptail --title "$form_title"  --inputbox "$(gettext "$form_domain_admin")" 10 60 3>&1 1>&2 2>&3)
if [ "$?" != "0" ] ; then
		exit 1
fi
adminpasswd=$(whiptail --title "$form_title"  --passwordbox "$(gettext "$form_pwd_domain_admin")" 10 60 3>&1 1>&2 2>&3)
if [ "$?" != "0" ] ; then
		exit 1
fi
kinit_exit_code=1
domain="$( echo "$domain" | sed -e "s/ //g" | tr "[:upper:]" "[:lower:]")"
admindudom="$( echo "$admindudom" | tr "[:upper:]" "[:lower:]")"
realm="$( echo $domain | sed -e "s/ //g" | tr "[:lower:]" "[:upper:]")"
netbiosnamedomaine="$( echo $realm | cut -d "." -f1 )"
systemctl stop smbd 
systemctl stop sssd
hostname=$(hostname)


sed -i "s/127.0.0.1.*/127.0.0.1	localhost/g" /etc/hosts
sed -i "s/127.0.1.1.*/127.0.1.1	$hostname.$realm $hostname/g" /etc/hosts


{
echo "
[sssd]
services = nss, pam
config_file_version = 2
domains = $domain
[pam]
offline_credentials_expiration = 0
[domain/$domain]
ad_domain = $domain
krb5_realm = $realm
cache_credentials = true
krb5_store_password_if_offline = True
id_provider = ad
access_provider = ad
override_homedir = /home/%d/%u
default_shell = /bin/bash"
} > /etc/sssd/sssd.conf

chown root:root /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf

{
echo "
[libdefaults]
default_realm = ${realm}
ticket_lifetime = 24h
renew_lifetime = 7d
"
} > /etc/krb5.conf

{
echo "
#======================= Global Settings =======================

[global]

workgroup = $netbiosnamedomaine 
client signing = yes
client use spnego = yes
kerberos method = secrets and keytab
realm = $realm
security = ads
"
} > /etc/samba/smb.conf



{
echo '
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference and `info packages installed, try:
# `info libc "Name Service Switch" for information about this file.

passwd:         compat sss 
group:          compat sss
shadow:         compat sss
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis sss
sudoers:        files sss
'
} > /etc/nsswitch.conf

## création des profile a la voler par pam authentification

{
echo '
Name: my_mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
 required pam_mkhomedir.so umask=0077 skel=/etc/skel
'
} > /usr/share/pam-configs/my_mkhomedir


pam-auth-update --package my_mkhomedir


sleep 2

systemctl restart ntp 
systemctl start smbd 
systemctl start sssd

message_error=$(echo "$adminpasswd" | kinit $admindudom 2>&1>/dev/null )
kinit_exit_code=$?

if [ "$kinit_exit_code" = "0" ] ; then
	break
else
    whiptail --title "$form_title" --msgbox "$(gettext "$message_error")" 10 60 3>&1 1>&2 2>&3
fi
done

sleep 2

## ajout de la machine au domaine
message_error=$(net ads join -k 2>&1>/dev/null )
whiptail --title "$form_title" --msgbox "$(gettext "$message_error")" 10 60 3>&1 1>&2 2>&3

./install_autoupdatedns.sh
./install_ad_userlogin.sh

./install_conf_firefox_public.sh

## on supprime la syncro et scripte d'ouverture de sessions pour les postes isolée
rm -f /etc/cron.d/rsync_cdc
rm -rf /usr/local/share/rsyncweb/
rm -f /usr/local/bin/web_usersscript
rm -f /etc/xdg/autostart/web_usersscript.desktop

systemctl restart cron

cat lightdm.conf.ad > /etc/lightdm/lightdm.conf

systemctl restart lightdm 


#!/bin/bash
cp dnsautoupdate /usr/local/bin/ 
chmod +x /usr/local/bin/dnsautoupdate
{
echo '[Unit]
Description=Scripts personalisée dnsautoupdate dns
After=network-online.target
User=root
WorkingDirectory=/root/

[Service]
ExecStart=/usr/local/bin/dnsautoupdate
Type=oneshot

[Install]
WantedBy=default.target

'
} > /etc/systemd/system/dnsautoupdate.service
chmod 664 /etc/systemd/system/dnsautoupdate.service
{
echo "
[Unit]
Description=on lance un dnsautoupdate toutes les heures

[Timer]
# on attent 5 minute après le boot
OnBootSec=5min
# toute les 15 minutes 
OnUnitActiveSec=15min
# Autoriser la persistence entre les reboot
Persistent=true
[Install]
WantedBy=timers.target
"
} > /etc/systemd/system/dnsautoupdate.timer
chmod 664 /etc/systemd/system/dnsautoupdate.timer
systemctl daemon-reload
systemctl enable dnsautoupdate.timer
systemctl start dnsautoupdate.timer
#!/bin/bash

filconf="install.conf"
fqdnsrvwebrsync="$( grep "^fqdnsrvwebrsync" $filconf | cut -d "=" -f2 )"
usersshwebrsync="$( grep "^usersshwebrsync" $filconf | cut -d "=" -f2 )"
homeusersshwebrsync="$( grep "^homeusersshwebrsync" $filconf | cut -d "=" -f2 )"
portssh="$( grep "^portssh" $filconf | cut -d "=" -f2 )"
firefoxesrlocalsettingsjs="$( grep "^firefoxesrlocalsettingsjs" $filconf | cut -d "=" -f2 )"
firefoxesrlocalconf="$( grep "^firefoxesrlocalconf" $filconf | cut -d "=" -f2 )"
glpipem="$( grep "^glpipem" $filconf | cut -d "=" -f2 )"
glpiagentcfg="$( grep "^glpiagentcfg" $filconf | cut -d "=" -f2 )"
privkeywebrsync="$( grep "^privkeywebrsync" $filconf | cut -d "=" -f2 )"
scriptxdgskelperso="$( grep "^scriptxdgskelperso" $filconf | cut -d "=" -f2 )"
# sur install de base debian10 xfce amd64
dirscript="$(dirname $(readlink -f $0))"
#version 0.5

## nomage du poste , basé sur la premier adresse mac trouver sur le poste cela evite les doublond de nom de machine sur le domain.
## TYPE de poste -TYPE d'OS - Les 6 dernier chiffre de la Mac Adresse
## UC/SRV  L/W/M aabbcc00
mac=$(ip addr | grep link/ether | awk '{print $2}' | sed '1q;d' | sed -e "s/://g")
hostnamectl set-hostname "UC-L-${mac:6}"
sed -i "s/127.0.1.1.*/127.0.1.1	UC-L-${mac:6}/g" /etc/hosts

# parramétrage des depots indispansable pour un clients linux.
# deb-multimedia.org  pour flash-player , backports pour les derniaire verssion logiciel libreoffice,vlc,firefox-esr
# main contrib non-free pour le reste .

{
echo 'deb http://ftp.fr.debian.org/debian/ buster main contrib non-free
deb http://security.debian.org/debian-security buster/updates main contrib non-free
# buster-updates, previously known as 'volatile'
deb http://ftp.fr.debian.org/debian/ buster-updates main contrib non-free
## depots deb-multimedia.org
deb http://www.deb-multimedia.org buster main non-free
## depots backports pour avoir les versions plus récente de certin logiciels
deb http://ftp.debian.org/debian buster-backports main contrib non-free
'
} > /etc/apt/sources.list
apt-get update -oAcquire::AllowInsecureRepositories=true
sleep 1
apt-get install deb-multimedia-keyring
sleep 1
apt-get update -oAcquire::AllowInsecureRepositories=false


## parramétrage de la priorisations des dépots pour garder une verssion stable après un dist-upgrade.
rm -f /etc/apt/preferences.d/buster

rm -f /etc/apt/preferences.d/ubuntubionic

{
echo 'Package: *
Pin: release o=Unofficial Multimedia Packages
Pin-Priority: 400
'
} > /etc/apt/preferences.d/multimedia

{
echo '
Package: *
Pin: release a=buster-backports
Pin-Priority: 400
Package: vlc
Pin: release a=buster-backports
Pin-Priority: 999

Package: python3-uno
Pin: release a=buster-backports
Pin-Priority: 999

Package: uno-libs3
Pin: release a=buster-backports
Pin-Priority: 999

Package: ure
Pin: release a=buster-backports
Pin-Priority: 999

Package: libreoffic*
Pin: release a=buster-backports
Pin-Priority: 999

Package: fonts-*
Pin: release a=buster-backports
Pin-Priority: 999

Package: kicad*
Pin: release a=buster-backports
Pin-Priority: 999

Package: firefox-esr*
Pin: release a=buster-backports
Pin-Priority: 999
'
} > /etc/apt/preferences.d/backports

# ajout u support multiarchitecture pour les applications type teamviewer ...
export os_architecture=$(dpkg --print-architecture)
if [ $os_architecture = "amd64" ];then
dpkg --add-architecture i386
fi

# update de de l'os
apt-get update
apt-get -y dist-upgrade
# nétoyage pour l'ibérrer de l'esspace après update.
apt-get -y autoremove --purge
apt-get clean

# install des paquets du clients.
apt-get -y install gettext zenity unzip p7zip sudo ntp dnsutils rsync firmware-linux xserver-xorg xfce4 xfce4-notes-plugin xfce4-pulseaudio-plugin xfce4-terminal \
 xfce4-weather-plugin xfce4-places-plugin xfce4-mount-plugin xfce4-battery-plugin xfce4-clipman-plugin xfce4-datetime-plugin \
 thunar-archive-plugin thunar-volman thunar-media-tags-plugin lightdm light-locker numlockx gdebi-core \
 firefox-esr-l10n-fr vlc \
 evince poppler-utils \
 libreoffice-core libreoffice-base libreoffice-l10n-fr libreoffice-help-fr \
 libreoffice-writer libreoffice-calc libreoffice-impress \
 libreoffice-draw libreoffice-math libreoffice-base \
 network-manager openjdk-11-jre ristretto cron-apt \
 chameleon-cursor-theme gtk2-engines-aurora gnome-brave-icon-theme gnome-disk-utility gnome-calculator \
 fusioninventory-agent openssh-server photoflare gthumb gnome-keyring seahorse evolution \
 gvfs-backends jmtpfs go-mtpfs


apt-get clean
if [ -f $glpipem ];then
cp $glpipem /etc/fusioninventory/
fi
if [ -f $glpiagentcfg ];then
cp $glpiagentcfg /etc/fusioninventory/agent.cfg
/usr/bin/fusioninventory-agent
fi
# install de la gestion des imprimantes et scanaire , peut néssésiter l'ajout de drivers supplémentaire suivant les imprimantes, scannaire ...
apt-get -y install cups simple-scan
 
# ajout de la derniaire version flashplayeur ( depot multimedia indispenssable) 
apt-get -y install flashplayer-mozilla
# install chrommium fr + flashplayer
./install_chrome.sh

#apt-get -y install network-manager-openvpn-gnome 

if [ $(ls teamviewer*.deb | wc -l) -ge 1 ];then
./install_teamviewer12.sh
fi

if [ -f $scriptxdgskelperso ];then
$scriptxdgskelperso
fi

apt-get clean
apt-get -y autoremove --purge

#prompte rouge pour root

echo "PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\# '" >> /root/.bashrc

# parametrage 
if [ -f $firefoxesrlocalsettingsjs ];then
   if [ -f $firefoxesrlocalconf ];then
      cp -f $firefoxesrlocalsettingsjs /usr/lib/firefox-esr/defaults/pref/
      cp -f $firefoxesrlocalconf /usr/lib/firefox-esr/
   fi
fi

# interdiction au autres utilisateurs de naviguer dans les répertoirs personnels
# pour la commande useradd
sed -i "s/^UMASK.*/UMASK           027/g" /etc/login.defs
# pour la commande adduser
sed -i "s/^DIR_MODE.*/DIR_MODE=0750/g" /etc/adduser.conf
sed -i "s/^[E|#].*XTRA_GROUPS=.[a-Z].*/EXTRA_GROUPS=\"dialout cdrom floppy audio video plugdev users scanner netdev nas dip \"/g" /etc/adduser.conf
sed -i "s/^[A|#].*DD_EXTRA_GROUPS=.*/ADD_EXTRA_GROUPS=1/g" /etc/adduser.conf
#pour les comptes deja créée
cd /home/
for dir in *
do
chmod 750 $dir
done
cd "$dirscript"


## configuration des mise a jour systèmes.
{
	echo '
# Configuration for cron-apt. For further information about the possible
# configuration settings see /usr/share/doc/cron-apt/README.gz.
APTCOMMAND=/usr/bin/apt-get
OPTIONS="-o quiet=1"
MAILON="never"
SYSLOGON="always" '
} > /etc/cron-apt/config

{
echo '
autoclean -y
autoremove -y --purge
dist-upgrade -y -o APT::Get::Show-Upgraded=true
'
} > /etc/cron-apt/action.d/3-download 

{
echo '
#
# Regular cron jobs for the cron-apt package
#
# Every 2 hour.
 0 */2	* * *	root test -x /usr/sbin/cron-apt && /usr/sbin/cron-apt /etc/cron-apt/config
# Every five minutes.
# */5 *	* * *	root test -x /usr/sbin/cron-apt && /usr/sbin/cron-apt /etc/cron-apt/config
'
} > /etc/cron.d/cron-apt 

service cron restart

sed -i "s/^GRUB_TIMEOUT.*/GRUB_TIMEOUT=0/g" /etc/default/grub
update-grub

if [ -f $privkeywebrsync ];then
## syncro webrsync
mkdir /root/.ssh
chmod 700 /root/.ssh
cp ${privkeywebrsync} /root/.ssh/
chown -R root:root /root/.ssh
chmod 600 /root/.ssh/${privkeywebrsync}
mkdir -p /usr/local/share/rsyncweb/

{
echo "
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user  command
*/15 *    * * * root  rsync --delete --chmod 755 -av -e \"ssh -o StrictHostKeyChecking=no -i /root/.ssh/${privkeywebrsync} -p ${portssh}\" ${usersshwebrsync}@${fqdnsrvwebrsync}:${homeusersshwebrsync} /usr/local/share/rsyncweb/ > /dev/null 2>&1
"

} > /etc/cron.d/rsync_web
systemctl restart cron
fi
## install du service d'execution de scripte a l'ouverture du system diffuséer soit par un ad soit par webrsync
{
echo '[Unit]
Description=Scripts personalisée ouverture système
After=network.target

[Service]
ExecStart=/usr/local/bin/sysscript
Type=oneshot

[Install]
WantedBy=graphical.target

'
} > /etc/systemd/system/sysscript.service
chmod 664 /etc/systemd/system/sysscript.service

{
echo '#!/bin/bash
##### ne pas modifier cette partie #####
## execution des sysscript difusée par webrsync
if [ -f /usr/local/share/rsyncweb/linux/sysscript ] ;then
chmod +x /usr/local/share/rsyncweb/linux/sysscript
/usr/local/share/rsyncweb/linux/sysscript
fi
## execution des sysscript difusée par AD
if [ -f /usr/local/etc/netlogon/linux/sysscript ] ;then
chmod +x /usr/local/etc/netlogon/linux/sysscript
/usr/local/etc/netlogon/linux/sysscript
fi
########################################

## ajouter vos commandes ci après.

'
} > /usr/local/bin/sysscript
chmod 755 /usr/local/bin/sysscript

systemctl daemon-reload
systemctl enable sysscript

cp web_usersscript /usr/local/bin/ 
chmod 755 /usr/local/bin/web_usersscript

cp web_usersscript.desktop /etc/xdg/autostart/

apt-get -y install sudo cifs-utils keyutils libpam-script rsync

## on configure pam_script
{
echo 'Name: Support for authentication by external scripts
Default: yes
Priority: 257
Auth-Type: Primary
Auth:
       sufficient                      pam_script.so
Account-Type: Primary
Account:
       sufficient                      pam_script.so
#Password-Type: Primary
#Password:
#       sufficient                      pam_script.so
Session-Type: Additional
Session:
       optional                        pam_script.so

'
} > /usr/share/pam-configs/pam_script
## on aplique la configuration de pam_script
pam-auth-update --package pam_script

groupadd nas

sed -i "/%nas ALL=(ALL:ALL) NOPASSWD:\/sbin\/mount.cifs/d" /etc/sudoers
echo "%nas ALL=(ALL:ALL) NOPASSWD:/sbin/mount.cifs //*/* /media/partages/*/*/*" >> /etc/sudoers

sed -i "/%nas ALL=(ALL:ALL) NOPASSWD:\/bin\/umount/d" /etc/sudoers

echo "%nas ALL=(ALL:ALL) NOPASSWD:/bin/umount /media/partages/*/*/*" >> /etc/sudoers

rm -f /etc/xdg/autostart/mount_nat.desktop
cp -f mount_nas.desktop /etc/xdg/autostart/

cp -f mount_nas /usr/local/bin/

rm -f /etc/xdg/autostart/UserlogonScript.desktop
cp -f UserlogonScript.desktop /etc/xdg/autostart/

cp -f UserlogonScript /usr/local/bin/

chmod 755 /usr/local/bin/UserlogonScript

cp -f pam_script* /usr/share/libpam-script/

chmod -R 755 /usr/share/libpam-script/*

rm -f /usr/share/libpam-script/pam_script_auth

cat lightdm.conf > /etc/lightdm/lightdm.conf


service lightdm restart

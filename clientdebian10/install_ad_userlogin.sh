#!/bin/bash

netlogonsmbrep=${netlogonsmbrep:="/media/netlogon/"}
netlogonlocalrep=${netlogonlocalrep:="/usr/local/etc/netlogon/"}

apt-get  -y install zenity gettext

cp ad_usersscript /usr/local/bin/ 
chmod 755 /usr/local/bin/ad_usersscript
cp ad_usersscript.desktop /etc/xdg/autostart/

### ajout des droit pour monter les lecteur réseaux


sed -i "/%domain\\\ admins ALL=(ALL:ALL) ALL/d" /etc/sudoers
echo "%domain\ admins ALL=(ALL:ALL) ALL" >> /etc/sudoers

sed -i "/%Administrateur\\\ locaux ALL=(ALL:ALL) ALL/d" /etc/sudoers
echo "%Administrateur\ locaux ALL=(ALL:ALL) ALL" >> /etc/sudoers

sed -i "/%domain\\\ users ALL=(ALL:ALL) NOPASSWD:\/sbin\/mount.cifs/d" /etc/sudoers
echo "%domain\ users ALL=(ALL:ALL) NOPASSWD:/sbin/mount.cifs //*/* /media/partages/*/*/*" >> /etc/sudoers
echo "%domain\ users ALL=(ALL:ALL) NOPASSWD:/sbin/mount.cifs //*/netlogon/ $netlogonsmbrep *" >> /etc/sudoers

sed -i "/%domain\\\ users ALL=(ALL:ALL) NOPASSWD:\/bin\/umount/d" /etc/sudoers
echo "%domain\ users ALL=(ALL:ALL) NOPASSWD:/bin/umount $netlogonsmbrep" >> /etc/sudoers
echo "%domain\ users ALL=(ALL:ALL) NOPASSWD:/bin/umount /media/partages/*/*/*" >> /etc/sudoers

sed -i "/%domain\\\ users ALL=(ALL:ALL) NOPASSWD:\/usr\/bin\/rsync -a/d"  /etc/sudoers
echo "%domain\ users ALL=(ALL:ALL) NOPASSWD:/usr/bin/rsync -a $netlogonsmbrep $netlogonlocalrep"  >> /etc/sudoers 

cp ad_passwordnew /usr/local/bin/ 
cp ad_passwordnew_fr_FR.mo /usr/share/locale/fr/LC_MESSAGES/ad_passwordnew.mo
chmod 755 /usr/local/bin/ad_passwordnew

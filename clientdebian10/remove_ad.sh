apt-get -y autoremove --purge ctparental privoxy dnsmasq e2guardian clamav*
apt-get -y autoremove --purge realmd sssd sssd-tools adcli  krb5-user packagekit samba
domain="$( grep "^domain" $filconf | cut -d "=" -f2 )"
## nomage du poste , basé sur la premier adresse mac trouver sur le poste cela evite les doublond de nom de machine sur le domain.
## TYPE de poste -TYPE d'OS - Les 6 dernier chiffre de la Mac Adresse
## UC/SRV  L/W/M aabbcc00
mac=$(ip addr | grep link/ether | awk '{print $2}' | sed '1q;d' | sed -e "s/://g")
hostnamectl set-hostname "UC-L-${mac:6}"
sed -i "s/127.0.1.1.*/127.0.1.1	UC-L-${mac:6}/g" /etc/hosts

rm -f /usr/local/bin/ad_usersscript 

{
echo '
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference and `info packages installed, try:
# `info libc "Name Service Switch" for information about this file.

passwd:         compat 
group:          compat
shadow:         compat
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
sudoers:        files
'
} > /etc/nsswitch.conf

rm -rf /home/$domain/



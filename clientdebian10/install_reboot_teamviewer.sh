cp -f tvreboot /usr/local/bin/
chmod +x /usr/local/bin/tvreboot

apt-get install -y patch
{
echo '[Unit]
Description=Scripts pour redémaret teamviewer quant il plante
After=network-online.target
User=root
WorkingDirectory=/root/

[Service]
ExecStart=/usr/local/bin/tvreboot
Type=oneshot

[Install]
WantedBy=default.target

'
} > /etc/systemd/system/rebootteamviewer.service

chmod 664 /etc/systemd/system/rebootteamviewer.service
{
echo "
[Unit]
Description=on lance rebootteamviewer.service toutes les 2 minutes

[Timer]
# on attent 5 minute après le boot
OnBootSec=5min
# toute les 2 minutes 
OnUnitActiveSec=2min
# Autoriser la persistence entre les reboot
Persistent=true
[Install]
WantedBy=timers.target
"
} > /etc/systemd/system/rebootteamviewer.timer
chmod 664 /etc/systemd/system/rebootteamviewer.timer
systemctl daemon-reload
systemctl enable rebootteamviewer.timer
systemctl start rebootteamviewer.timer

if [ $(grep -c "numlockx" /etc/lightdm/lightdm.conf | grep -v "^#") -ge 1 ];then

sed -i "s/^greeter-setup-script=.*/greeter-setup-script=\/usr\/bin\/numlockx on/g" /etc/lightdm/lightdm.conf
sed -i "s/^session-setup-script=.*/#session-setup-script=/g" /etc/lightdm/lightdm.conf
sed -i "s/^session-cleanup-script=.*/session-cleanup-script=\/usr\/bin\/numlockx on/g" /etc/lightdm/lightdm.conf

else

sed -i "s/^greeter-setup-script=.*/#greeter-setup-script=/g" /etc/lightdm/lightdm.conf
sed -i "s/^session-setup-script=.*/#session-setup-script=/g" /etc/lightdm/lightdm.conf
sed -i "s/^session-cleanup-script=.*/#session-cleanup-script=/g" /etc/lightdm/lightdm.conf

fi

systemctl restart lightdm



#!/bin/bash
sed -i "s/greeter-setup-script=\/usr\/bin\/numlockx on/greeter-setup-script=\/usr\/bin\/numlockx off/g" /etc/lightdm/lightdm.conf
sed -i "s/session-cleanup-script=\/usr\/bin\/numlockx on/session-cleanup-script=\/usr\/bin\/numlockx off/g" /etc/lightdm/lightdm.conf
sed -i "s/numlockx on/numlockx off/g" /usr/local/bin/UserlogonScript

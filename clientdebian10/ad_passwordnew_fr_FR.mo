��          �      �       H  d   I  J   �  1   �     +     >     M     _     y     �  ;   �  A   �  1   6  8   h     �  r   �  X   5  0   �     �     �     �  %     &   (  &   O  `   v  9   �  B     9   T                    
                  	                           8 characters minimum, 1 uppercase, 1 lowercase,
1 special character and must not be an old password! Access denied: Try a more complex password, or contact your administrator. Close and reopen the session to raise the shares. Current Password : New Password : Password changed. Repeat your new password: You must change your password. You must re-type your password. kinit: No credentials found with supported encryption types kpasswd: Generic preauthentication failure getting initial ticket kpasswd: Password mismatch while reading password kpasswd: Preauthentication failed getting initial ticket Project-Id-Version: 
PO-Revision-Date: 2020-02-05 19:12+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
 8 caractères minimum, 1 majuscule, 1 minuscule
1 caractère spécial et ne doit pas être un ancien mot de passe! Accès refusé: essayez un mot de passe plus complexe ou contactez votre administrateur. Re-lancer la session pour accéder aux partages. Mot de passe actuel : Nouveau mot de passe : Mot de passe changé. Répétez votre nouveau mot de passe: Vous devez changer votre mot de passe. Vous devez retaper votre mot de passe. kinit: aucune information d'identification trouvée avec les types de chiffrement pris en charge kpasswd: le Mots de passe actuel rentré n'est pas le bon kpasswd: les nouveaux mots de passe rentré ne sont pas identiques kpasswd: le Mots de passe actuel rentré n'est pas le bon 
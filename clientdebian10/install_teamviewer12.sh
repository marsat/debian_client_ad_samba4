# installation teamviewer
export os_architecture=$(dpkg --print-architecture)
if [ $os_architecture = "amd64" ]
then
apt-get -y install libjpeg62-turbo:i386
else
apt-get -y install libjpeg62-turbo
fi
gdebi --non-interactive teamviewer*.deb
cp -f tvreboot /usr/local/bin/
chmod +x /usr/local/bin/tvreboot
apt-get install -y patch
{
echo '[Unit]
Description=Scripts pour redémaret teamviewer quant il plante
After=network-online.target
User=root
WorkingDirectory=/root/

[Service]
ExecStart=/usr/local/bin/tvreboot
Type=oneshot

[Install]
WantedBy=default.target

'
} > /etc/systemd/system/rebootteamviewer.service
chmod 664 /etc/systemd/system/rebootteamviewer.service
{
echo "
[Unit]
Description=on lance rebootteamviewer.service toutes les 2 minutes

[Timer]
# on attent 5 minute après le boot
OnBootSec=5min
# toute les 2 minutes 
OnUnitActiveSec=2min
# Autoriser la persistence entre les reboot
Persistent=true
[Install]
WantedBy=timers.target
"
} > /etc/systemd/system/rebootteamviewer.timer
chmod 664 /etc/systemd/system/rebootteamviewer.timer
systemctl daemon-reload
systemctl enable rebootteamviewer.timer
systemctl start rebootteamviewer.timer

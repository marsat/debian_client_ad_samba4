#!/bin/bash
apt-get -y install chromium chromium-l10n flashplayer-chromium


## patch du lanseur chromium pour ne pas utiliser les trouseaux de clef et ainssi eviter les messages demandent un mots de pass a l'ouverture de chromium.

for file in $(find /etc/skel/ -type f -name "*.desktop" 2>/dev/null)
do
    sed -i "s/^Exec=\/usr\/bin\/chromium .*/Exec=\/usr\/bin\/chromium --password-store=basic %U/g" $file
done
for file in $(find /home/ -type f -name "*.desktop" 2>/dev/null)
do
   sed -i "s/^Exec=\/usr\/bin\/chromium .*/Exec=\/usr\/bin\/chromium --password-store=basic %U/g" $file
done
sed -i "s/^Exec=\/usr\/bin\/chromium .*/Exec=\/usr\/bin\/chromium --password-store=basic %U/g" /usr/share/applications/chromium.desktop

